package com.example.mycounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int mCount = 0;
    private TextView mShowCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //find the View that has ID of the TextView that shows the count.
        mShowCount = findViewById(R.id.textView_count);
    }

    public void countUp(View view) {
        mCount++;
        if (mShowCount != null)
            mShowCount.setText(String.valueOf(mCount));
    }

    public void reset(View view) {
        mCount = 0;
        if (mShowCount != null)
            mShowCount.setText(String.valueOf(mCount));

        Toast toast = Toast.makeText(this,
                R.string.toast_message,
                Toast.LENGTH_SHORT);
        toast.show();
    }

    public void countDown(View view) {
        mCount--;
        if (mShowCount != null)
            mShowCount.setText(String.valueOf(mCount));
    }

    public void times2(View view) {
        mCount = 2 * mCount;
        if (mShowCount != null)
            mShowCount.setText(String.valueOf(mCount));
    }
}